=========================
PHOTO FRAME DRUPAL MODULE
=========================
by Freakk
ffr3akk@gmail.com
http://www.freakk.net
http://drupal.org/user/2762953

Maintainers:
  Freakk (http://drupal.org/user/2762953)


CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Features
  * Installation
  * Known Issues
  * To do


INTRODUCTION
------------
Photo Frame is a Drupal module that displays a slideshow of images with a custom frame overlay.
Image list is dynamically generated froma user-defined folder.

FEATURES
------------
- User-defined images folder
- Automatic image list
- Selectable frame overlay

INSTALLATION
------------
1. Copy photo_frame folder to modules directory.
2. At admin/build/modules enable the photo_frame module.
4. Configure the module at admin/config/content/photo_frame.

KNOWN ISSUES
------------
No known issues reported.

